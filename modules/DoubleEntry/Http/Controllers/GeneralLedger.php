<?php

namespace Modules\DoubleEntry\Http\Controllers;

use App\Http\Controllers\Controller;
use Date;
use Modules\DoubleEntry\Models\Account;
use Modules\DoubleEntry\Models\DEClass;
use Modules\DoubleEntry\Models\Ledger;

class GeneralLedger extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        list($ledgers) = $this->getData();

        // Check if it's a print or normal request
        if (request('print')) {
            $template = 'print';
        } else {
            $template = 'index';
        }

        return view('double-entry::general-ledger.' . $template, compact('ledgers'));
    }

    public function export()
    {
        \Excel::create(trans('double-entry::general.general_ledger'), function($excel) {
            $excel->sheet(trans('double-entry::general.general_ledger'), function($sheet) {
                $template = 'body';

                list($ledgers) = $this->getData();

                $sheet->loadView('double-entry::general-ledger.' . $template, compact('ledgers'));
            });
        })->download('xlsx');
    }

    protected function getData()
    {
        // Get year
        $year = request('year', Date::now()->year);

        $limit = request('limit', setting('general.list_limit', '25'));

        $ledgers = Ledger::with(['account'])->orderBy('issued_at', 'desc')->paginate($limit);

        return [$ledgers];
    }
}
