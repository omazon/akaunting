<?php

namespace Modules\DoubleEntry\Observers\DoubleEntry;

use App\Models\Banking\Transfer;
use App\Models\Income\Revenue;
use App\Models\Expense\Payment;
use App\Models\Setting\Category;
use Modules\DoubleEntry\Models\Journal as Model;
use Modules\DoubleEntry\Models\AccountBank;
use Modules\DoubleEntry\Models\Ledger;

class Journal
{
    /**
     * Listen to the created event.
     *
     * @param  Model  $journal
     * @return void
     */
    public function created(Model $journal)
    {
        if (str_contains($journal->reference, 'transfer:')) {
            return;
        }

        $input = request()->input();

        foreach ($input['item'] as $item) {
            $bank = AccountBank::where('account_id', $item['account_id'])->first();

            if (empty($bank)) {
                continue;
            }

            $request = [
                'company_id' => $journal->company_id,
                'account_id' => $bank->bank_id,
                'paid_at' => $journal->paid_at,
                'currency_code' => setting('general.default_currency'),
                'currency_rate' => '1',
                'description' => $journal->description,
                'payment_method' => setting('general.default_payment_method'),
                'reference' => 'journal-entry:' . $journal->id,
            ];

            if (!empty($item['credit'])) {
                $request['amount']      = $item['credit'];
                $request['category_id'] = Category::where('type', 'expense')->enabled()->pluck('id')->first();

                Payment::create($request);
            } else {
                $request['amount']      = $item['debit'];
                $request['category_id'] = Category::where('type', 'income')->enabled()->pluck('id')->first();

                Revenue::create($request);
            }
        }
    }

    /**
     * Listen to the created event.
     *
     * @param  Model  $journal
     * @return void
     */
    public function updated(Model $journal)
    {
        $input = request()->input();

        foreach ($input['item'] as $item) {
            if (!empty($item['credit'])) {
                $class = '\App\Models\Expense\Payment';
                $field = 'credit';
            } else {
                $class = '\App\Models\Income\Revenue';
                $field = 'debit';
            }

            $record = $class::where('reference', 'journal-entry:' . $journal->id)->first();

            if (empty($record)) {
                continue;
            }

            $ledger = Ledger::record($journal->id, get_class($journal))->whereNotNull($field)->first();

            $bank = AccountBank::where('account_id', $ledger->account_id)->first();

            if (!empty($bank)) {
                $record->paid_at = $journal->paid_at;
                $record->amount = $item[$field];
                $record->description = $journal->description;

                $record->save();
            } else {
                $bank = AccountBank::where('account_id', $item['account_id'])->first();

                if (!empty($bank)) {
                    $record->account_id = $bank->bank_id;
                    $record->paid_at = $journal->paid_at;
                    $record->amount = $item[$field];
                    $record->description = $journal->description;

                    $record->save();
                } else {
                    $record->delete();
                }
            }
        }

        if (str_contains($journal->reference, 'transfer:')) {
            $transfer_id = str_replace('transfer:', '', $journal->reference);
            $transfer = Transfer::where('id', $transfer_id)->first();

            if (!empty($transfer)) {
                $transfer->amount = $journal->amount;
                $transfer->paid_at = $journal->paid_at;
                $transfer->description = $journal->description;
                $transfer->save();
            }
        }
    }

    /**
     * Listen to the deleted event.
     *
     * @param  Model  $journal
     * @return void
     */
    public function deleted(Model $journal)
    {
        Payment::where('reference', 'journal-entry:' . $journal->id)->delete();
        Revenue::where('reference', 'journal-entry:' . $journal->id)->delete();

        if (str_contains($journal->reference, 'transfer:')) {
            $transfer_id = str_replace('transfer:', '', $journal->reference);
            Transfer::where('id', $transfer_id)->delete();
        }
    }
}
