<?php

namespace Modules\DoubleEntry\Observers\Company;

use App\Models\Company\Company as Model;
use Artisan;

class Company
{
    /**
     * Listen to the created event.
     *
     * @param  Model  $company
     * @return void
     */
    public function created(Model $company)
    {
        // Create seeds
        Artisan::call('doubleentry:seed', [
            'company' => $company->id
        ]);
    }

    /**
     * Listen to the deleted event.
     *
     * @param  Model  $company
     * @return void
     */
    public function deleted(Model $company)
    {
        $tables = [
            'double_entry_accounts', 'double_entry_account_bank', 'double_entry_account_tax', 'double_entry_classes',
            'double_entry_journals', 'double_entry_ledger', 'double_entry_types',
        ];

        foreach ($tables as $table) {
            $this->deleteItems($company, $table);
        }
    }

    /**
     * Delete items in batch.
     *
     * @param  Model  $company
     * @param  $table
     * @return void
     */
    protected function deleteItems($company, $table)
    {
        foreach ($company->$table as $item) {
            $item->delete();
        }
    }
}