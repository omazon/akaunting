<?php

return [

    'accounting'            => 'Accounting',
    'double_entry'          => 'Double-Entry',
    'chart_of_accounts'     => 'Chart of Accounts',
    'journal_entry'         => 'Journal Entry',
    'general_ledger'        => 'General Ledger',
    'balance_sheet'         => 'Balance Sheet',
    'trial_balance'         => 'Trial Balance',
    'account'               => 'Account',
    'debit'                 => 'Debit',
    'credit'                => 'Credit',
    'total_type'            => 'Total :type',

];
