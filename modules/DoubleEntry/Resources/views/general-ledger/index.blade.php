@extends('layouts.admin')

@section('title', trans('double-entry::general.general_ledger'))

@section('new_button')
    <span class="new-button"><a href="{{ route('general-ledger.index') }}?print=1&year={{ request('year', $this_year) }}" target="_blank" class="btn btn-default btn-sm"><span class="fa fa-print"></span> &nbsp;{{ trans('general.print') }}</a></span> <span><a href="{{ route('general-ledger.export') }}?print=1&year={{ request('year', $this_year) }}" class="btn btn-default btn-sm"><span class="fa fa-file-excel-o"></span> &nbsp;{{ trans('general.export') }}</a></span>
@endsection

@section('content')<!-- Default box -->
    <div class="box box-success">
        @include('double-entry::general-ledger.body')

        <div class="box-footer">
            @include('partials.admin.pagination', ['items' => $ledgers, 'type' => 'transactions'])
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
@endsection
