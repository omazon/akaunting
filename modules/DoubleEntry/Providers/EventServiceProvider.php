<?php

namespace Modules\DoubleEntry\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\DoubleEntry\Listeners\AdminMenuCreated;
use Modules\DoubleEntry\Listeners\ModuleInstalled;
use Modules\DoubleEntry\Listeners\Version1012;
use Modules\DoubleEntry\Listeners\Version1015;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['events']->listen(\App\Events\AdminMenuCreated::class, AdminMenuCreated::class);
        $this->app['events']->listen(\App\Events\ModuleInstalled::class, ModuleInstalled::class);
        $this->app['events']->listen(\App\Events\UpdateFinished::class, Version1012::class);
        $this->app['events']->listen(\App\Events\UpdateFinished::class, Version1015::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}