<?php

namespace Modules\DoubleEntry\Models;

use App\Models\Model;

class Account extends Model
{

    protected $table = 'double_entry_accounts';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['debit_total', 'credit_total'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['company_id', 'type_id', 'code', 'name', 'description', 'parent', 'system', 'enabled'];

    public function type()
    {
        return $this->belongsTo('Modules\DoubleEntry\Models\Type');
    }

    public function bank()
    {
        return $this->belongsTo('Modules\DoubleEntry\Models\AccountBank', 'id', 'account_id');
    }

    public function tax()
    {
        return $this->belongsTo('Modules\DoubleEntry\Models\AccountTax', 'id', 'account_id');
    }

    public function ledgers()
    {
        return $this->hasMany('Modules\DoubleEntry\Models\Ledger');
    }

    /**
     * Scope code.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $code
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCode($query, $code)
    {
        return $query->where('code', $code);
    }

    /**
     * Get the name including rate.
     *
     * @return string
     */
    public function getDebitTotalAttribute()
    {
        return $this->ledgers()->sum('debit');
    }

    /**
     * Get the name including rate.
     *
     * @return string
     */
    public function getCreditTotalAttribute()
    {
        return $this->ledgers()->sum('credit');
    }
}
